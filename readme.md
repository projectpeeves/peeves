# Peeves

- API (Based on Laravel 5)

# Setup environment

- Fetch code, duh
- Follow below steps

## Install Required Programs
- Install WAMP Server
  -- http://www.wampserver.com/en/

- Install Composer
  -- https://getcomposer.org/download/

## Install Dependencies

Run below commands on CMD 

- When in `peeves` folder, `composer install`
- When in `peeves` folder, `php artisan key:generate`

## Database Setup 

- Create `/api/.env` file using content of `/api/.env.sample`
- Add a `peeves` database to Apache MySQL,
- Update the database name and password in `/api/.env` file

## Add below entry to end of `hosts` file in Windows
127.0.0.1    api-local.peeves.com

### Set virtual host for API project in `wamp64/bin/apache/apache2.4.17/conf/extra/httpd-vhosts.conf`. (Do not forget to fix directory names as per your local directory structure and remember to uncomment the vhosts lines in httpd.conf)
```
<VirtualHost *:80>
    ServerAdmin <YOUR_EMAIL>
    DocumentRoot "YOUR_DRIVE:/wamp64/www/peeves/public"
    ServerName api-local.peeves.com
    ErrorLog "logs/error.log"

   	<Directory "YOUR_DRIVE:/wamp64/www/peeves/public">
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
        Order allow,deny
        allow from all
   </Directory>
</VirtualHost>
```
- Check response for the same by navigating to `https://api-local.peeves.com' in browser.
- Browser will most probably throw warning about safety, click on proceed / I understand the risk etc. & it will allow the access. This happens because our SSL certificate is self-signed & not well recognized by browser. 

## WAMP issue debug
- Open CMD in YOUR_DRIVE:\wamp\bin\apache\apache2.2.8\bin and enter httpd -t to check issue.

## CORS issue

- Enable Headers modules in Apache

## cURL issue

- Download thread safe fixed php_curl extension from http://www.mediafire.com/file/3ay381k3cq59cm2/php_curl-5.4.3-VC9-x64.zip & copy paste in your php lib folder. Ref : http://www.anindya.com/php-5-4-3-and-php-5-3-13-x64-64-bit-for-windows
- If you are using WAMP, you may have to go to php.ini of php folder & enable cURL extension manually as WAMP uses different php.ini than system,

## Migration commands

- To create a migration `php artisan make:migration create_users_table`
- Running all outstanding migrations or if migrations table doen't exist in your DB `php artisan migrate`
- Rollback last migration operation `php artisan migrate:rollback`
- Rollback all migrations `php artisan migrate:reset`
- Rollback all migrations and run them all again `php artisan migrate:refresh`
- Rollback all migrations and run them all again with database seeding `php artisan migrate:refresh --seed`

## Seeding commands

- To seed data `php artisan db:seed`
- To seed data for particular table `php artisan db:seed --class=ClassName` ( NOTE: If any other table is dependant on the table in which you have seeded the data, then you have to seed the data of that table first.)

## ngrok commands

- Download ngrok and unzip.

- Copy ngrok.exe file to C:/wamp64

- Open command prompt in C:/wamp64 and run the cummands:

  - `ngrok authtoken 7uCSFfmYHdKo95PzpkDQN_2tNt1LanpduzXRUBnjRay`
  - `ngrok http -subdomain=<YOUR_NAME> -host-header=api-local.peeves.com 80`

- Check response by navigating to `<YOUR_NAME>.ngrok.io`

  ​