<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'ses' => [
        'key'    => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'messages'  => [
        'interview' => 'Thank you for your interest in Skarma. Looking forward to getting in touch with you soon!',
        'lead'  => 'We have registered your request. Looking forward to getting in touch with you soon!'
    ],

    'facebook' => [
        'page_url'          => 'https://www.facebook.com/peevesbot/',
        'verify_token'      => 'peevestoken',
        'page_access_token' => 'EAACUb96h6d8BAEkJJmSdz0x8zGoI7DxiZCpcp6qyUsxmRLP4jrDU1PFEZCjglqu06DvTKQXD5YrCrQTkhj5SzZBlnhl8qWUu1XaZCbEZAXwQlNGWRP1H4UkRgNY9dpZBnNUTTwlQUNgXBZByL2THIRZBeoWD4X0MuGEYM95qFAJ2QwZDZD',

        'text_character_limit' => 319,

        'failed_message'    => 'Sorry, but I am not able to comprehend what you are saying! I am just a bot, you see! Could you ask me that in another way, please?'

    ],

    'apiai' => [
        'client_access_token' => 'ec4d5f9a04e24a479661fc31e733655e',
        'developer_access_token' => '9c321c72fac747babb4892c7185980eb',
        'base_url'    => 'https://api.api.ai/v1/'
    ],

    'uber' => [
        'base_url'  => 'https://sandbox-api.uber.com/v1.2/',
        'client_id' => 'PV4DqUz0_5YZ46i2tD-F53V0zSZ203iw',
        'client_secret' => 'PEAgbVuNhcGKAvYF8G2m-iQj0DhVDdcP4kDNxGqy',
        'access_token'  => 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZXMiOlsiaGlzdG9yeSIsImhpc3RvcnlfbGl0ZSIsInBsYWNlcyIsInByb2ZpbGUiLCJyaWRlX3dpZGdldHMiXSwic3ViIjoiMmU3YzRmYjAtNTZjMy00MzUyLWFjNmQtMmRmMjlhYTQ4YjQ0IiwiaXNzIjoidWJlci11czEiLCJqdGkiOiJhNjliMDYyMC1lNGFhLTRlZGEtYTZjNy0wZTBlYzhmNzY5Y2YiLCJleHAiOjE0OTA4NTE3MDksImlhdCI6MTQ4ODI1OTcwOSwidWFjdCI6IlZnT3Q1OWJScEdXS0t0aHppTmlwMHF4b0tZZEpicCIsIm5iZiI6MTQ4ODI1OTYxOSwiYXVkIjoiUFY0RHFVejBfNVlaNDZpMnRELUY1M1YwelNaMjAzaXcifQ.JvugbtFf8P8EqvWOzL7GfLVE4AsIvxLajGNLG9LRD0pFzWFN3iJm-I_9JoQRUs8f-nA5LsKbc6X_IlKHr8FxmfkbDQVLxFtnFN90R_P7pLBcQg8CiiIGVTB5X-l6lrLYd4YnZ6B1yLVbECtQqIXVch6NMsP5oi5wk9MTKqegOAMP7aHvDAfpkEt4TSWSt4AWRpwvh20M2zUaFaOi82CYeRRa0HCYd-jdkfYD_YRN6uK4az7T7QHnwMPGbmJMlg26MQpobGtg59OQzZf5TYg_bmJSfAz3cOL_eZJo5QOrPSM_jSW2HYiaqPuBx9Ja9E7ynqhRTj3Mn8LHiWUjKx-F3g',
        'server_token'  => '5tb1uxYbBopPMrxyfUZmOBBCqEe7jygXXKYJHsrT'
    ],

    'gmaps' => [
        'key'   => 'AIzaSyBKwgodug_c_Jr9Q9XPVbTnSd1TgKxFHrc'
    ]

];
