<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiaiResponse extends Model
{
	protected $fillable = ['facebook_request_id', 'action_incomplete', 'action', 'parameters', 'fulfillment', 'intent_name', 'session_id'];
    protected $table = 'apiai_responses';

    public static $custom_action_namespace = 'peeves';

    public function hasIntentName() {
        return !empty($this->intent_name);
    }

    public function hasAction() {
        return !empty($this->action);
    }

    public function hasCustomActionNamespace() {
        $namespace = explode('.', $this->action)[0];
        return $namespace == self::$custom_action_namespace;
    }

    public function hasFulfillment() {
        return !empty(json_decode($this->fulfillment));
    }
}
