<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\FacebookRequest;
use DB;

class FacebookRequestController extends Controller
{
    public static function storeFacebookRequests($request)
    {

        $facebook_requests = collect();
        $entry = $request['entry'][0];

        if(!is_null($entry)) {
            foreach ($entry['messaging'] as $messaging) {
                if(FacebookRequest::hasMessageText($messaging)) {

                    $facebook_request = FacebookRequest::findByMid($messaging['message']['mid']);

                    // Check if response was already given for this request
                    if(!empty($facebook_request)) {
                        if($facebook_request->hasResponses()) {
                            continue; // We don't want to process requests we already replied to
                        } 
                    } else {
                        $facebook_request = new FacebookRequest([
                            'sender_id'     => $messaging['sender']['id'], 
                            'page_id'       => $messaging['recipient']['id'], 
                            'timestamp'     => $messaging['timestamp'], 
                            'text'          => $messaging['message']['text'], 
                            'mid'           => $messaging['message']['mid'], 
                            'seq'           => $messaging['message']['seq']
                        ]); 
                    }
                    
                    $facebook_request->save();
                    $facebook_requests->push($facebook_request);
                    
                    /*Log::info('========= FacebookRequest - START =============');
                    Log::info($facebook_request);
                    Log::info('========= FacebookRequest - END =============');*/
                }
            }
        }

        return $facebook_requests;
    }
}
