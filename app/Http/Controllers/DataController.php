<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\UberRideController;
use App\FacebookUser;
use Config;


class DataController extends Controller
{
     public static function queryData($apiai_response, $facebook_request) 
    {
        $parameters = json_decode($apiai_response->parameters);

        $data = [
            'texts' => [
                'Not sure about that one. Sorry!'
            ]
        ];

        switch($apiai_response->action) {
            case 'peeves.book_taxi': {
            	/*if(!empty($parameters->date_time)&&!empty($parameters->end_address)&&!empty($parameters->start_address)) {
            		UberRideController::scheduleRequest($facebook_request->sender_id,$parameters->start_address,$parameters->end_address,$parameters->date_time);
            		$data = [
                        'texts'     => [
                            'We will show some data soon.'
                        ]
                    ];
            	}*/

            	if(!empty($parameters->end_address)&&!empty($parameters->start_address)) {
            		$collections = UberRideController::rideRequest($facebook_request,$parameters->start_address,$parameters->end_address);
            		$data = [
                        'collections' => $collections
                    ];
            	} 

            	else {
            		$fulfillment = json_decode($apiai_response->fulfillment);
                    $data = [
                        'texts'     => [
                            $fulfillment->speech
                        ]
                    ];
            	}

            }
        }

        return $data;
    }
}
