<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacebookRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use DB;

class FacebookResponseController extends Controller
{
    
    public static function storeFacebookResponses($facebook_responses)
    {
        foreach($facebook_responses as $facebook_response) {
            if(is_array($facebook_response->collections)) {
                $facebook_response->collections = json_encode($facebook_response->collections);
            }

            if(is_array($facebook_response->buttons)) {
                $facebook_response->buttons = json_encode($facebook_response->buttons); 
            }

            $facebook_response->save();
        }

        return $facebook_responses;
    }
}
