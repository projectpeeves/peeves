<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use App\FacebookResponse;
use App\FacebookRequest;
use Stevenmaguire;
use Config;
use Session;

class UberRideController extends Controller
{

    /*public function __construct(){
        $uber_client = new Uber\Client(array(
            'access_token' => Config::get('services.uber.access_token'),
            'server_token' => Config::get('services.uber.server_token'),
            'use_sandbox'  => true,
            'version'      => 'v1.2',
            'locale'       => 'en_US',
        ));
    }*/

    public static function rideRequest($fare_id, $start_address, $end_address){

        $start_loc  = self::getLocationLatLng($start_address);
        $end_loc    = self::getLocationLatLng($end_address);

        Log::info('=======Start_LOC=======');
        Log::info($start_loc);
        Log::info('=======End_LOC=======');
        Log::info($end_loc);
        //self::getAuthentication();
        $result = self::getProductList($start_loc, $end_loc);
        return $result;
    }

    public static function getAuthentication($sender_id){

        $collections = [];
        $buttons = [];

        array_push($buttons,
            [
                'type' => 'web_url',
                'title' => 'Uber Log In',
                'url' => 'https://login.uber.com/oauth/v2/authorize?client_id=PV4DqUz0_5YZ46i2tD-F53V0zSZ203iw&response_type=code',
            ]);

        array_push($collections,
        [
            'title'     => "Log In to Book your Ride",
            'image_url' => 'https://d1a3f4spazzrp4.cloudfront.net/facebook/hd_shareimage.png',
            'subtitle'  => 'I need you to log in for me please',
            'buttons'   => $buttons
        ]);

        $data = [
            'collections' => $collections
        ];
        FacebookResponse::init();

        $facebook_request=new FacebookRequest();
        $facebook_request->id = '999999';

        Log::info('Type:'.gettype($facebook_request));

        $facebook_responses = FacebookResponse::processData($data, $facebook_request);
        foreach($facebook_responses as $facebook_response) {
               $facebook_response->sendFacebookMessage($sender_id);
            }
    }

    public static function scheduleRequest($fare_id, $start_address, $end_address, $date_time){
     

    }

    public static function getLocationLatLng($address){
        $loc_client = new Client([
            'base_uri'  => 'https://maps.googleapis.com/maps/api/',
            'headers'   => [
                'Authorization' => 'Bearer '.Config::get('services.apiai.client_access_token')
            ]
        ]);

        $gmaps_param_array = array(
                'address'   => $address,
                'key'       => Config::get('services.gmaps.key')
            );


        $loc_response = $loc_client->request('GET', 'geocode/json'.'?'.http_build_query($gmaps_param_array));



        $data = json_decode($loc_response->getBody());
        $results = $data->results[0];

        return [
            'location'  => $address,
            'lat'   => $results->geometry->location->lat,
            'lng'   => $results->geometry->location->lng
        ];
    }

    public static function getProductList($start_address, $end_address){

        $uber_client = new StevenMaguire\Uber\Client(array(
            'access_token' => Config::get('services.uber.access_token'),
            'server_token' => Config::get('services.uber.server_token'),
            'use_sandbox'  => true,
            'version'      => 'v1.2',
            'locale'       => 'en_US',
        ));

        $products = $uber_client->getTimeEstimates(array(
            'start_latitude'  => $start_address['lat'],
            'start_longitude' => $start_address['lng'],
        ));


        Log::info("=========Uber Response=========");
        $data=$products->times;
        Log::info($data);
        //Log::info(gettype($data));
        $elements = [];
        foreach ($data as $cabs) {
            Log::info("=========localized_display_name=========");
            Log::info($cabs->localized_display_name);
            Log::info($cabs->estimate);
            Log::info($cabs->product_id);

            $requestEstimate = $uber_client->getRequestEstimate(array(
                'product_id' => $cabs->product_id,
                'start_latitude' => $start_address['lat'],
                'start_longitude' => $start_address['lng'],
                'end_latitude' => $end_address['lat'], // optional
                'end_longitude' => $end_address['lng'], // optional
            ));

            $surge_text = '';
            if(property_exists($requestEstimate, 'fare')){
                $price_estimate = $requestEstimate->fare->display;
                $fare_id = $requestEstimate->fare->fare_id;
            } elseif(property_exists($requestEstimate, 'estimate')){
                $price_estimate =$requestEstimate->estimate->display;
                $surge_text = 'Surge applicable is x'.$requestEstimate->estimate->surge_multiplier;
                $confirmation_url = $requestEstimate->estimate->confirmation_href;
            }

            $buttons = [];

            if(isset($fare_id)){
                
                $payload_data = array(
                    'fare_id'       => $fare_id,
                    'product_id'    => $cabs->product_id,
                    'start_lat'     => $start_address['lat'],
                    'start_lng'     => $start_address['lng'],
                    'end_lat'     => $end_address['lat'],
                    'end_lng'     => $end_address['lng']
                    );

                $json_payload = json_encode($payload_data);

                array_push($buttons,
                [
                    'type' => 'postback',
                    'title' => 'Book the '.$cabs->localized_display_name,
                    'payload' => $json_payload,
                ]);    
            } else if(isset($fare_id)){
                array_push($buttons,
                [
                    'type' => 'web_url',
                    'title' => 'Book the '.$cabs->localized_display_name,
                    'url' => $confirmation_url,
                ]);
            }

            array_push($elements,
            [
                'title'     => $cabs->localized_display_name." (".$price_estimate.")",
                'image_url' => 'https://deveshr.github.io/images/'.$cabs->localized_display_name.'.jpg',
                'subtitle'  => ((string)($cabs->estimate)/60).' minutes away. '.$surge_text,
                'buttons'   => $buttons
            ]);
        }
        return $elements;
    }

    public static function bookRide($sender_id,$fare_id, $product_id, $start_lat, $start_lng, $end_lat, $end_lng){

        session_start();
        self::getAuthentication($sender_id);

        $auth_token = null;
        $count = 100;
        while(is_null($auth_token)&&$count!=0){
            $auth_token = $_SESSION["auth_token"];
            Log::info("==============Authorization Token=============");
            Log::info($auth_token); 
            $count--;
            sleep(2);
        }
        Log::info("==============Authorization Token Out=============");
        Log::info($auth_token); 
        session_abort();
/*        $uber_client = new StevenMaguire\Uber\Client(array(
            'access_token' => $auth_token,
            'server_token' => Config::get('services.uber.server_token'),
            'use_sandbox'  => true,
            'version'      => 'v1.2',
            'locale'       => 'en_US',
        ));

        $request = $uber_client->requestRide(array(
            'start_latitude' => $start_lat,
            'start_longitude' => $start_lng,
            'end_latitude' => $end_lat,
            'end_longitude' => $end_lng,
            'fare_id' => $fare_id
        ));*/

        //Log::info($request);        
    }
}
