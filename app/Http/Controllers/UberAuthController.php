<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Session;
use Stevenmaguire;
use Config;

class UberAuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        session_start();
        Log::info('index hit');
        $inputs = Input::all();
        $code = $inputs['code'];
        Log::info('========== Code - START ==========');
        Log::info($code);
        Log::info('========== Code - END ==========');

        $provider = new Stevenmaguire\OAuth2\Client\Provider\Uber(array(
            'clientId'          => Config::get('services.uber.client_id'),
            'clientSecret'      => Config::get('services.uber.client_secret'),
            'redirectUri'       => 'https://9f502b11.ngrok.io/v1/uberwebhook'
        ));

        $token = $provider->getAccessToken('authorization_code', [
            'code' => $_GET['code']
        ]);

        Log::info('============Token============');
        Log::info($token);
        Log::info('==========Token - end==========');
        //Session::set('auth_token',$token);
        $_SESSION["auth_token"]=$token;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        Log::info('create hit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        Log::info('store hit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        Log::info('show hit');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        Log::info('edit hit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Log::info('update hit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Log::info('destroy hit');
    }
}
