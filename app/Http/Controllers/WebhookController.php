<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Response;
use App\Http\Controllers\FacebookRequestController;
use App\Http\Controllers\FacebookResponseController;
use App\Http\Controllers\FacebookUserController;
use App\Http\Controllers\ApiaiResponseController;
use App\Http\Controllers\ApiaiContextController;
use App\Http\Controllers\DataController;
use App\Http\Controllers\UberRideController;
use App\FacebookRequest;
use App\FacebookUser;
use App\FacebookResponse;
use Config;

class WebhookController extends Controller
{
    public function index()
    {
        $inputs = Input::all();
        $verify_token = $inputs['hub_verify_token'];

        if(!is_null($verify_token) && $verify_token === Config::get('services.facebook.verify_token')) {
            echo $inputs['hub_challenge'];
        }
    }

    public function store(Request $request)
    {   
        FacebookUser::init();
        FacebookResponse::init();
        $facebook_responses = collect();

        Log::info('========= RawFacebookRequest - START =============');
        Log::info($request);
        Log::info('========= RawFacebookRequest - END =============');

        if(array_key_exists('postback', $request['entry'][0]['messaging'][0])){
                Log::info("Got a Postback");
                $postback_data = json_decode($request['entry'][0]['messaging'][0]['postback']['payload']);
                Log::info(gettype($postback_data));
                UberRideController::bookRide($request['entry'][0]['messaging'][0]['sender']['id'],$postback_data->fare_id,$postback_data->product_id,$postback_data->start_lat,$postback_data->start_lng,$postback_data->end_lat, $postback_data->end_lng);
        }
        
        else if(array_key_exists('message', $request['entry'][0]['messaging'][0])){
        Log::info("Got a Message");
        // TODO : Save facebook delivery notifications

        // Save all facebook requests
        $facebook_requests = FacebookRequestController::storeFacebookRequests($request);

        // Process individual facebook requests to determine what the response should be
        foreach($facebook_requests as $facebook_request) {
            
            // NLP + ML on facebook request  
            $raw_apiai_response = $facebook_request->queryTextWithApiai();
            $apiai_response = ApiaiResponseController::storeApiaiResponse($raw_apiai_response, $facebook_request);

            // If action is defined with custom namespace, that signifies we need to pass control to DataController
            if($apiai_response->hasAction() && $apiai_response->hasCustomActionNamespace()) {

                //Log::info('========= Custom action namespace =============');
                $data = DataController::queryData($apiai_response, $facebook_request, false);
                $facebook_responses = FacebookResponse::processData($data, $facebook_request); 

            } else if ($apiai_response->hasFulfillment()) {    // If custom namespace is absent, we should have an answer from one of the custom domains enabled OR we may have defined it ourselves

                Log::info('========= Domain namespace =============');
                $fulfillment = json_decode($apiai_response->fulfillment);
                $facebook_responses = FacebookResponse::processData($fulfillment, $facebook_request, true);

            } else {    // We don't have any response available, let's apologize 
                
                Log::info('========= No match =============');
                $facebook_response = new FacebookResponse($facebook_request);
                $facebook_response->text = Config::get('services.facebook.failed_message');
                $facebook_responses->push($facebook_response);

            }
                
            // Process responses gathered
            foreach($facebook_responses as $facebook_response) {
               $facebook_response->sendFacebookMessage();
            }
        }
        FacebookResponseController::storeFacebookResponses($facebook_responses);
        FacebookUserController::storeFacebookUsers($facebook_requests);
    }

    else
        {
            Log::info("Try Again");
        }
    }
}
