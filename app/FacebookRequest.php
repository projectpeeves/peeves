<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use Config;

class FacebookRequest extends Model
{
	protected $fillable = ['sender_id', 'timestamp', 'text', 'page_id', 'mid', 'seq'];
    protected $table = 'facebook_requests';

    public function facebookResponses() {    
        return $this->hasMany('App\FacebookResponse');
    }

    public function facebookUser() {    
        return $this->belongsTo('App\FacebookUser');
    }

    public function queryTextWithApiai() {
        if(!empty($this->text)) {
            $client = new Client([
                'base_uri'  => Config::get('services.apiai.base_url'),
                'headers'   => [
                    'Authorization' => 'Bearer '.Config::get('services.apiai.client_access_token')
                ]
            ]);
            $response = $client->request('GET', 'query', [
                'query' => [
                    'query' => $this->text,
                    'v'     => '20150910',
                    'lang'  => 'en',
                    'sessionId' => "{$this->sender_id}"
                ]
            ]);

            Log::info('========= RawApiaiResponse - START =============');
            Log::info($response->getBody());
            Log::info('========= RawApiaiResponse - END =============');

            Log::info('========= RawApiaiResponse - START =============');
            Log::info(gettype($response->getBody()));
            Log::info('========= RawApiaiResponse - END =============');
            return json_decode($response->getBody());
        }
    }

    public function hasResponses() {
        return sizeof($this->facebookResponses) > 0;
    }

    public static function hasMessageText($messaging) {
        return array_key_exists('message', $messaging) && array_key_exists('text', $messaging['message']);
    }

    public static function hasDeliveryNotification($messaging) {
        return array_key_exists('delivery', $messaging);
    }

    public static function findByMid($mid) {
        return FacebookRequest::where('mid', '=', $mid)->first();
    }
}
